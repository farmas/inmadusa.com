Inmadusa = Inmadusa || {};

(function () {

    var dict = {};

    Inmadusa.getResources = function (lang, rootKey) {
        var resourceKey = rootKey + '-' + lang;

        if (!dict[resourceKey]) {
            var templateResources = resources[rootKey] || {};
            var languageResources = {};

            Object.keys(templateResources).forEach(function (key) {
                languageResources[key] = templateResources[key][lang] || 'N/A';
            });

            dict[resourceKey] = languageResources;
        }

        return dict[resourceKey];
    }

    Inmadusa.findResourceByValue = function (rootKey, lang, val) {
        var templateResources = resources[rootKey] || {}

        var match = Object.keys(templateResources).find(function (key) {
            var resource = templateResources[key]
            return resource && resource[lang] === val;
        });

        return match && {
            key: match,
            en: templateResources[match]['en'],
            es: templateResources[match]['es']
        }
    }

    var resources = {
        routes: {
            inicio: {
                es: 'inicio',
                en: 'home'
            },
            empresa: {
                es: 'empresa',
                en: 'company'
            },
            localizacion: {
                es: 'localizacion',
                en: 'location'
            },
            productos: {
                es: 'productos',
                en: 'products'
            },
            servicios: {
                es: 'servicios',
                en: 'services'
            },
            contacto: {
                es: 'contacto',
                en: 'contact'
            }
        },
        layout: {
            inicio: {
                es: 'Inicio',
                en: 'Home'
            },
            empresa: {
                es: 'Empresa',
                en: 'Company'
            },
            localizacion: {
                es: 'Localización',
                en: 'Location'
            },
            productos: {
                es: 'Productos',
                en: 'Products'
            },
            servicios: {
                es: 'Servicios',
                en: 'Services'
            },
            contacto: {
                es: 'Contacto',
                en: 'Contact'
            }
        },
        inicio: {
            mainTitle: {
                es: 'Inyección de Plastico',
                en: 'Injection Molding'
            },
            mainDescription: {
                es: 'La empresa nace en el año de 1985 en la ciudad de Durango, con el objetivo de fabricar-maquilar piezas de plástico para la industria automotriz y otras aplicaciones.',
                en: 'The Company was founded in 1985 in the city of Durango, with the aim of manufacturing plastic parts for the automotive industry and other applications.'
            },
            summaryMaquila: {
                es: 'Maquila de piezas de plástico para diversa aplicación.',
                en: 'Manufacturing of plastic parts and components for different applications.'
            },
            summaryReloj: {
                es: 'Tiempos y lugar de entrega según requerimiento y solicitud del cliente.',
                en: 'Delivery time and place according to customer needs and requirements.'
            },
            summaryEmpaque: {
                es: 'Producto empacado según especificación del cliente.',
                en: 'Product packing according to customer specifications.'
            },
            summaryIso: {
                es: 'Certificación de calidad en la norma ISO 9001:2015',
                en: 'Quality certification for ISO 9001:2015'
            },
            productos: {
                es: 'Productos',
                en: 'Products'
            },
            noticias: {
                es: 'Noticias',
                en: 'News'
            },
            noticiasCertificacion: {
                es: 'Certificación en',
                en: 'Quality certification for'
            },
            noticiasDescripcion: {
                es: 'Certificación realizada por Perry Johnson Registrars, Inc. acreditada por <b>ema</b> con el número de acreditación 44/12.',
                en: 'Certification performed by Perry Johnson Registrars, Inc. accredited by <b>ema</b> with the accreditation number 44/12.'
            },
            clientes: {
                es: 'Clientes',
                en: 'Customers'
            }
        },
        empresa: {
            historiaTitle: {
                es: 'Historia',
                en: 'Our Story'
            },
            historiaDescription: {
                es: '<p>La empresa nace en el año de 1985 en la ciudad de Durango, con el objetivo de fabricar-maquilar piezas de plástico para la industria automotriz y otras aplicaciones.</p>' +
                    '<p>Se ha operado bajo la misma dirección y como un negocio familiar para la inyección de plástico que ha ido creciendo a través del tiempo.</p>' +
                    '<p>En INMADUSA la calidad de los productos, el servicio y compromiso con el cliente son el principal objetivo de trabajo.</p>' +
                    '<p>Estamos enfocados hacia la mejora continua para seguir desarrollándonos y ampliar nuestra cartera de clientes permitiéndonos la consolidación como proveedores oficiales y de confianza con el cliente.</p>',
                en: '<p>The company was founded in 1985 in the city of Durango, with the aim of manufacturing plastic parts for the automotive industry and other applications.</p>' +
                    '<p>The family business initiative, has been growing in experience and expertise along with the years to become a settled company.</p>' +
                    '<p>In INMADUSA, our goals are to deliver top product quality, excellent service and permanent commitment to the customer.</p>' +
                    '<p>We are focused on the continuous improvement of our work processes in order to continue the business development and grow customer portfolio; this will allow INMADUSA to establish as a permanent trusted supplier.</p>'
            },
            empresaTitle: {
                es: 'Empresa',
                en: 'Our Company'
            },
            empresaDescription: {
                es: 'Se ha desarrollado y mantenido un enfoque al giro automotriz fabricando como empresa Tier 2 y Tier 3, piezas para calefactores y sistemas de aire acondicionado en automóviles de distintas OEM´s tales como:',
                en: 'INMADUSA has developed and maintained expertise in the automotive sector as a Tier 2 & Tier 3 supplier, by manufacturing components and parts for the heating and air conditioning system. Some of our products go to companies such as:'
            }
        },
        localizacion: {
            direccion: {
                es: '<p>Zona industrial, Boulevard Francisco Villa.</p>' +
                    '<p>Fácil y rápido acceso a carreteras hacia Saltillo Coahuila, Monterrey N.L., Aguascalientes, San Luis Potosi, Guanajuato, México.</p>',
                en: '<p>Industrial área., Francisco Villa Blvd.</p>' +
                    '<p>Easy access of highways to Saltillo Coahuila, Monterrey N.L., Aguascalientes, San Luis Potosi, Guanajuato, Mexico.</p>' +
                    '<p>Km 2.5 carretera Torreón, Fracc. La Cima C.P. 34204 Durango, Dgo.</p>'
            }
        },
        productos: {
            materiaTitle: {
                es: 'Materia Prima',
                en: 'Raw Material'
            },
            materiaSubtitle: {
                es: 'Tipos de Materia Prima utilizada:',
                en: 'Types of raw material used:'
            },
            materia1: {
                es: 'Polioximetileno',
                en: 'Polyoxymethylene'
            },
            materia1Info: {
                es: '(POM): Delrin',
                en: '(POM): Delrin'
            },
            materia2: {
                es: 'Polipropileno',
                en: 'Polypropylene'
            },
            materia2Info: {
                es: '(PP): PP con o sin carga',
                en: '(PP): w/, w/o talc'
            },
            materia3: {
                es: 'Polietileno',
                en: 'Polyethylene'
            },
            materia3Info: {
                es: '(PE): HDPE, LDPE',
                en: '(PE): HDPE, LDPE'
            },
            materia4: {
                es: 'Poliamidas',
                en: 'Polyamides'
            },
            materia4Info: {
                es: '(PA): Nylon 66 + GF',
                en: '(PA): Nylon 66 + GF'
            },
            materiaSummary: {
                es: '<p>- Termoplásticos de ingeniería (específicos)</p>' +
                    '<p>- Pigmentos Master Batch según especificación</p>' +
                    '<p>- Entre otros, indicados por el cliente</p>',
                en: '<p>- Engineering thermoplastics (specific)</p>' +
                    '<p>- Master batch according to specification</p>' +
                    '<p>- More, indicated by customer</p>'
            },
            productos: {
                es: 'Productos',
                en: 'Products'
            },
            productosSubtitle: {
                es: 'Entre los productos que INMADUSA produce se encuentran:',
                en: 'Examples of manufactured parts:'
            },
            productosList: {
                es: '<p>- Componentes plásticos para la industria automotriz</p>' +
                    '<p>- Soportes</p>' +
                    '<p>- Compuertas</p>' +
                    '<p>- Tapones y Clips</p>' +
                    '<p>- Levas y Manivelas</p>' +
                    '<p>- Ductos</p>' +
                    '<p>- Ensambles</p>',
                en: '<p>- Cams, cranks and handles</p>' +
                    '<p>- Ducts</p>' +
                    '<p>- Assemblies</p>' +
                    '<p>- Gates and Doors</p>' +
                    '<p>- Caps, Plugs and Clips</p>'
            },
            productosSummary: {
                es: 'Entre muchos otros de figura compleja e irregular',
                en: 'Many other with complex and irregular forms'
            }
        },
        servicios: {
            serviciosTitle: {
                es: 'Servicios',
                en: 'Services'
            },
            serviciosSubtitle: {
                es: 'En INMADUSA ofrecemos los siguientes servicios:',
                en: 'Management and customer services:'
            },
            maquilaText: {
                es: 'Maquila de piezas de plástico para diversa aplicación.',
                en: 'Manufacturing of plastic parts and components for different applications.'
            },
            relojText: {
                es: 'Tiempos y lugar de entrega según requerimiento y solicitud del cliente.',
                en: 'Delivery time and place according to customer needs and requirements.'
            },
            empaqueText: {
                es: 'Producto empacado según especificación del cliente.',
                en: 'Product packing according to customer specifications.'
            },
            isoText: {
                es: 'Certificación de calidad en la norma ISO 9001:2015.',
                en: 'Quality certification for ISO 9001:2015.'
            },
            injeccionTitle: {
                es: 'Capacidad de Inyección',
                en: 'Injection Molding'
            },
            injeccionSubtitle: {
                es: 'Actualmente contamos con máquinas inyectoras en operación y disponibles, con diferente capacidad en tamaño y fuerza de cierre:',
                en: 'We currently hold 8 injection molding machines in operation with different clamping pressure capacities:'
            },
            injeccionSummary: {
                es: 'Desde <span>70</span> toneladas hasta <span>1000</span> toneladas de fuerza.',
                en: 'From <span>70</span> tons to <span>1000</span> tons of clamping pressure.'
            }
        },
        contacto: {
            contactoTitle: {
                es: 'Información de Contacto',
                en: 'Contact Information'
            },
            contactoDireccion: {
                es: 'Inyección y Maquila de Durango S.A. de C.V.<br />' +
                    'Km 2.5 Carretera Torreón, Fracc. La Cima<br />' +
                    'C.P. 34204 Durango, Dgo.<br />' +
                    '(618) 833 60 20<br />',
                en: 'Inyección y Maquila de Durango S.A. de C.V.<br />' +
                    'Km 2.5 Carretera Torreón, Fracc. La Cima<br />' +
                    'C.P. 34204 Durango, Dgo.<br />' +
                    '(618) 833 60 20<br />'
            }
        }
    }
})();