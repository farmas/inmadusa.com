Inmadusa = Inmadusa || {};

(function() {
    var defaultRoute = 'inicio';
    var menuActiveClass = 'c-nav__item--active';
    var currentLanguage = '';
    var currentRoute = '';
    var spanishLinks = getLinks('es');
    var englishLinks = getLinks('en');

    $(document).ready(function() {
        Path.map('#/:lang/:route').to(function() {
            var lang = this.params['lang'];
            var route = this.params['route'];
            var routeInfo = Inmadusa.findResourceByValue('routes', lang, route);

            transitionRoute(lang, routeInfo);
        });

        Path.root("#/es/inicio");

        Path.listen();
    });

    Inmadusa.clickLanguage = function(lang) {
        event.preventDefault();

        var route = currentRoute;

        if (lang === 'en') {
            var routeInfo = Inmadusa.findResourceByValue('routes', 'es', route);
            route = routeInfo['en'];
        }

        window.location.hash ='#/' + lang + '/' + route;
    }

    function transitionRoute(lang, routeInfo) {
        if ($('.content').length) {
            $('.content').fadeOut('fast', function() {
                handleRoute(lang, routeInfo && routeInfo.key);
                $('.content').fadeIn(function() {
                    initializeSliders();
                });
            });
        } else {
            handleRoute(lang, routeInfo && routeInfo.key);
            initializeSliders();
        }
    }

    function getLinks(lang) {
        var routes = Inmadusa.getResources(lang, 'routes');
        var menus = Inmadusa.getResources(lang, 'layout');

        return Object.keys(routes).map(function(key) {
            return {
                url: '#/' + lang + '/' + routes[key],
                text: menus[key],
                route: key
            };
        });
    }

    function getTemplateHtml(lang, templateName, partialVM) {
        var template = Inmadusa.templates[templateName];
        var vm = {
            lang: lang,
            res: Inmadusa.getResources(lang, templateName)
        };

        return template($.extend(vm, partialVM));
    }

    function renderLayout(lang, route) {
        var vm = {
            links: lang === 'es' ? spanishLinks : englishLinks,
            enableSpanishLink: lang === 'en',
            enableEnglishLink: lang === 'es'
        };

        vm.links.forEach(function(l) {
            l.isActive = l.route === route;
        });

        var layoutHtml = getTemplateHtml(lang, 'layout', vm);
        $('.main').html(layoutHtml);

        $('.c-nav__item').off('click');
        $('.c-nav__item').on('click', function() {
            $('.c-nav__item').removeClass(menuActiveClass);
            $(this).addClass(menuActiveClass);
        });
    }

    function handleRoute(lang, route) {
        currentRoute = route || defaultRoute;

        if (currentLanguage !== lang) {
            currentLanguage = lang;

            renderLayout(lang, route);
        }

        var contentHtml = getTemplateHtml(lang, currentRoute);
        $('.content').html(contentHtml);
    }

    function initializeSliders() {
        $('.inicio-bxslider').bxSlider({
            slideWidth: 1096,
            auto: true,
            onSliderLoad: function(){
                $('.inicio-bxslider').css('visibility', 'visible');
            }
        });

        $('.productos-bxslider').bxSlider({
            slideWidth: 800,
            auto: true
        });
    }

})();