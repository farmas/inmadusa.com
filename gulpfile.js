var gulp = require('gulp');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');
var bs = require('browser-sync').create();
var buster = require('gulp-cache-buster');
var hasher = require('gulp-hasher');
var rename = require('gulp-rename');

gulp.task('templates', function() {
    return gulp.src('templates/*.hbs')
    .pipe(handlebars({
      handlebars: require('handlebars')
    }))
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'Inmadusa.templates',
      noRedeclare: true, // Avoid duplicate declarations
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('js/'));
});

gulp.task('css', function() {
    return gulp.src('css/*.css')
    .pipe(concat('index.css'))
    .pipe(gulp.dest('./'))
    .pipe(hasher());
});

gulp.task('js', ['templates'], function() {
    return gulp.src('js/*.js')
    .pipe(hasher());
});

gulp.task('browser-sync', function() {
    return bs.init({
        server: {
            baseDir: './'
        }
    });
});

gulp.task('html', ['js', 'css'], function() {
    return gulp.src('index-src.html')
    .pipe(buster({
        assetRoot: __dirname,
        assetUrl: '/',
        env: 'production',
        hashes: hasher.hashes
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest('./'));
});

gulp.task('watch', function() {
    gulp.watch('templates/*.hbs', ['templates']);
    gulp.watch('css/*.css', ['css']);
    gulp.watch(['js/*.js', 'index.css', 'index-src.html'], ['html']);

    gulp.watch('index.html').on('change', bs.reload);
});

gulp.task('default', ['html', 'browser-sync', 'watch']);