# INMADUSA.COM #

Source code of inmadusa.com website.

# Setup for Development #

## Steps ##
* Install Node.js
* Run 'npm install' from root.
* Run 'npm run dev' from root.

## Description ##
* Gulp task precompiles the handlebar templates.
* Gulp task concatenates all .css files into a single file.
* Gulp task starts dev server and loads site on browser.
* Gulp task watches files and automatically reloads browser when any file changes.
